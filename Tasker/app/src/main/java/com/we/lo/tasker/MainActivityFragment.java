package com.we.lo.tasker;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Set;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public static Activity context = null;
    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    public void fillMain(){
        LinearLayout ll = (LinearLayout)getView().findViewById(R.id.main_container);
        Set<String> mts = DataHandler.GetMasterTasks();
        ll.removeAllViews();
        for (String mt:mts) {
            TextView tv = new TextView(context);
            tv.setText(mt.toCharArray(), 0, mt.toCharArray().length);
            tv.setTextSize(30);
            tv.setOnClickListener((View.OnClickListener) context);
            ll.addView(tv);
        }
    }
}
