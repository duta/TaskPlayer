package com.we.lo.tasker;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class DetailActivity extends Activity {

    private String mt;
    private Set<String> dts;
    private static int mpDone;
    private static Map.Entry pair;
    private static MediaPlayer mediaPlayer;
    private static boolean onPlay;
    private static Vibrator vb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                mt= null;
            } else {
                mt= extras.getString("mt");
            }
        } else {
            mt= (String) savedInstanceState.getSerializable("mt");
        }

        vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        setContentView(R.layout.activity_detail);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addDetailTask(View v){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (data == null) return;
        DataHandler.AddMP3(mt, data.getData().toString());
        fillMain();
    }

    @Override
    protected void onStart() {
        super.onStart();
        fillMain();
    }

    public void fillMain() {
        LinearLayout ll = (LinearLayout)findViewById(R.id.detail_container);
        dts = DataHandler.GetMP3s(mt);
        ll.removeAllViews();
        for (String dt:dts) {
            TextView tv = new TextView(this);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            Uri ff = Uri.parse(dt);
            mmr.setDataSource(this, ff);
            String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            long durationMs = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            long duration = durationMs / 1000;
            long h = duration / 3600;
            long m = (duration - h * 3600) / 60;
            long s = duration - (h * 3600 + m * 60);
            char[] se = (title + " - " + h + ":" + m + ":" + s).toCharArray();//
            tv.setText(se, 0, se.length);
            tv.setTextSize(20);
            ll.addView(tv);
        }
    }

    @TargetApi(16)
    public void startThread(View v) throws IOException {
        final ProgressBar pb = (ProgressBar) ((View) v.getParent()).findViewById(R.id.progressBar2);
        if (((Button)v).getText().toString().equals("Play")) {
            onPlay = true;
            final Button b = (Button)v;
            b.setText("Stop");
            final Map<Uri, String> map = new HashMap<Uri, String>();
            int max = 0;
            for (String dt:dts) {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                Uri ff = Uri.parse(dt);
                mmr.setDataSource(this, ff);
                String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                long durationMs = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
                map.put(ff, title);
                max += durationMs;
            }
            final int lastMax = max;
            final Handler mHandler = new Handler();
            final Iterator it = map.entrySet().iterator();
            if (!it.hasNext()) {
                b.setText("Play");
                return;
            }
            pair = (Map.Entry) it.next();
            final DetailActivity c = this;
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(getApplicationContext(), (Uri) pair.getKey());
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mpDone = mp.getDuration();
                    Map.Entry newPair = null;
                    String text = "";
                    if (it.hasNext()) {
                        newPair = (Map.Entry) it.next();
                        text = " and task "+newPair.getValue()+" started";
                    }
// prepare intent which is triggered if the
// notification is selected
                    Intent intent = new Intent(c, MainActivity.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
                    PendingIntent pIntent = PendingIntent.getActivity(c, (int) System.currentTimeMillis(), intent, 0);
// build notification
// the addAction re-use the same intent to keep the example short
                    Notification n  = new Notification.Builder(c)
                            .setContentTitle("Task "+pair.getValue()+" ended"+text)
                            .setContentText("Tasker")
                            .setContentIntent(pIntent)
                            .setSmallIcon(R.drawable.add)
                            .setAutoCancel(true).build();
                    NotificationManager notificationManager =
                            (NotificationManager) c.getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.notify((int) System.currentTimeMillis(), n);
                    // Vibrate for 500 milliseconds
                    vb.vibrate(500);

                    if (newPair!=null) {
                        pair = newPair;
                        try {
                            mp.reset();
                            mp.setDataSource(c, (Uri) newPair.getKey());
                            mp.prepare();
                            mp.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        onPlay = false;
                        b.setText("Play");
                        pb.setProgress(0);
                    }
                }
            });
            mpDone = 0;
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        pb.setMax(lastMax);
                        while (onPlay) {
                            // Update the progress bar
                            mHandler.post(new Runnable() {
                                public void run() {
                                    pb.setProgress(mpDone + mediaPlayer.getCurrentPosition());
                                }
                            });
                            sleep(1000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();
        } else {
            onPlay = false;
            mediaPlayer.stop();
            ((Button)v).setText("Play");
            pb.setProgress(0);
        }
    }
}
