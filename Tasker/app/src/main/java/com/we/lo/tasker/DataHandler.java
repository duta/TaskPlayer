package com.we.lo.tasker;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.ArraySet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Created by Long on 8/28/2015.
 */
public final class DataHandler {

    public static Activity context = null;
    public static final String PREFS_NAME = "TaskerPrefsMain";
    public static String debug;

    public final static Set<String> GetMasterTasks(){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        return settings.getStringSet("Main Magic Holderz", new ArraySet<String>());
    }

    public final static void AddMasterTask(String mt){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        Set<String> data = settings.getStringSet("Main Magic Holderz", new ArraySet<String>());
        data.add(mt);
        editor.putStringSet("Main Magic Holderz", data);
        editor.commit();
    }

    public final static Set<String> GetMP3s(String mt){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        Set<String> data = settings.getStringSet(mt, new ArraySet<String>());
        List<String[]> listed = new ArrayList<>(data.size());
        for (String ad:data) {
            listed.add(ad.split("\\|"));
        }
        Collections.sort(listed, compareDetail);
        Set<String> value = new ArraySet<String>();
        for (String[] ad:listed) {
            value.add(ad[1]);
        }
        return value;
    }

    public final static void AddMP3(String mt, String detailname){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        Set<String> data = settings.getStringSet(mt, new ArraySet<String>());
        List<String[]> listed = new ArrayList<>(data.size());
        for (String ad:data) {
            listed.add(ad.split("\\|"));
        }
        Collections.sort(listed, compareDetail);
        Set<String> value = new ArraySet<String>();
        int num = 1;
        for (String[] ad:listed) {
            value.add(ad[0]+"|"+ad[1]);
            num = (Integer.parseInt(ad[0])+1);
        }
        value.add(num+"|"+detailname);
        editor.putStringSet(mt, value);
        editor.commit();
    }

    static final Comparator<String[]> compareDetail =
            new Comparator<String[]>() {
                public int compare(String[] e1, String[] e2) {
                    return Integer.parseInt(e1[0]) - Integer.parseInt(e2[0]);
                }
            };
}
